const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = '1cf943abf0168cfa0ce761e24a3a7f15';

const getAuthToken = () => {
  return fetch(`${API_URL}authentication/token/new?api_key=${API_KEY}`)
         .then(response => response.json())
         .then(response => response.request_token)
         .catch(() => {
           return '';
         })
}

export const getAuth = async () => {
  const request_token = await getAuthToken();
  localStorage.request_token = request_token;
  window.location.href = `https://www.themoviedb.org/authenticate/${request_token}?redirect_to=${window.location.href}`;
}

export const getSession = () => {
  const request_token = window.localStorage.request_token;
  return fetch(`${API_URL}authentication/session/new?api_key=${API_KEY}&request_token=${request_token}`, {
    method: 'POST'
  }).then(response => response.json())
  .then(response => {
    sessionStorage.session_id = response.session_id;
    return response.session_id;
  })
  .catch(() => {
    return '';
  })
}

export const getMovies = (page=1) => {
  return fetch(`${API_URL}discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=${page}`)
        .then(respone => respone.json())
        .then(respone => {
          if (respone.results) {
            return respone;
          }
          throw new Error();
        })
        .catch(() => {
          return {results: [], total_pages: 1};
        });
}

export const markAsFavorite = (movieId, fav) => {
  const sessionId = sessionStorage.session_id;
  return fetch(`${API_URL}account/0001/favorite?api_key=${API_KEY}&session_id=${sessionId}`, {
    method: 'POST',
    headers: {
      "Content-Type": "application/json;charset=utf-8"
    },
    body: JSON.stringify({
      media_type: 'movie',
      media_id: movieId,
      favorite: fav
    })
  }).then(response => response.json())
  .catch(() => {
    return {status_code: -1, status_message: 'error_marking_fav'}
  });
}

export const getFavMovies = () => {
  const sessionId = sessionStorage.session_id;
  return fetch(`${API_URL}account/0001/favorite/movies?api_key=${API_KEY}&session_id=${sessionId}`)
         .then(response => response.json())
         .catch(() => {
          return {results: []};
         })
}

export const addToWatchList = (movieId, addBool) => {
  const sessionId = sessionStorage.session_id;
  return fetch(`${API_URL}account/0001/watchlist?api_key=${API_KEY}&session_id=${sessionId}`, {
    method: 'POST',
    headers: {
      "Content-Type": "application/json;charset=utf-8"
    },
    body: JSON.stringify({
      media_type: 'movie',
      media_id: movieId,
      watchlist: addBool
    })
  }).then(response => response.json())
  .catch(() => {
    return {success: false, status_message: 'error_adding_watchlist'}
  });
}

export const getWatchList = () => {
  const sessionId = sessionStorage.session_id;
  return fetch(`${API_URL}account/0001/watchlist/movies?api_key=${API_KEY}&session_id=${sessionId}`)
         .then(response => response.json())
         .catch(() => {
           return {results: []};
         })
}

export const searchMovie = (searchString, page) => {
  let url = `${API_URL}search/movie?api_key=${API_KEY}&query=${encodeURI(searchString)}`;
  if (page) {
    url += `&page=${page}`;
  }
  return fetch(url)
         .then(response => response.json())
         .then(response => {
           if (response.results) {
             return response;
           }
           throw new Error();
         })
         .catch(() => {
           return {results: [], total_pages:1};
         })
}