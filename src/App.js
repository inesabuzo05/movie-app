import './App.css';
import Header from './components/Header/Header.js';
import Center from './components/Center/Center.js';
import {getMovies, getSession, getAuth, searchMovie} from './api/API_Methods.js';
import { useState, useEffect } from 'react';

function App() {
  const [movies, setMovies] = useState([]);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(true);
  const [totalPages, setTotalPages] = useState(1);
  const [searched, setSearched] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      if (!localStorage.request_token) {
        getAuth();
        return;
      }
      if (!sessionStorage.session_id) {
        const session = await getSession();
        sessionStorage.session_id = session;
      }
      const moviesRes = await getMovies();
      setMovies(moviesRes.results);
      setTotalPages(moviesRes.total_pages);
      setIsLoading(false);
    }
    fetchData();
  }, []);

  const getNextPage = async () => {
    if (totalPages < page+1){
      return;
    }
    let moviesRes = {};
    if (searched == '') {
      moviesRes = await getMovies(page + 1);
    } else {
      moviesRes = await searchMovie(searched, page + 1);
    }
    setMovies([...movies, ...moviesRes.results]);
    setPage(page+1);
  }

  const debounce = (func, interval) => {
    var timeout;
    return function () {
      var context = this,
        args = arguments;
      var later = function () {
        timeout = null;
        func.apply(context, args);
      };
      clearTimeout(timeout);
      timeout = setTimeout(later, interval || 200);
    };
  }

  const search = debounce(async (searchString) => {
    if (searchString === '') {
      const moviesRes = await getMovies();
      setMovies(moviesRes.results);
      setTotalPages(moviesRes.total_pages);
      setSearched('');
      setPage(1);
      return;
    }
    const result = await searchMovie(searchString);
    setMovies(result.results);
    setTotalPages(result.total_pages);
    setSearched(searchString);
    setPage(1);
  }, 300);

  if (isLoading) {
    return <div className="loader-container"><img src="loader.gif" alt="Is loading"></img></div>
  }
  return (
    <div className="App">
      <Header search={search}/>
      <Center movies={movies} getNextPage={getNextPage}/>
    </div>
  );
}

export default App;
