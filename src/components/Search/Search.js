import React from 'react';
import './Search.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const Search = (props) => {
  const {search} = props;

  const handleKeyup = (event) => {
    search(event.target.value);
  }

  return <div className="Search" data-testid="Search">
    <span className="search-icon"><FontAwesomeIcon icon={faSearch}/></span>
    <input type="text" placeholder="Search movie here" onKeyUp={(e) => handleKeyup(e)}></input>
    <span className="clear-icon"></span>
  </div>
};

export default Search;
