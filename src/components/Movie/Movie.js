import React from 'react';
import './Movie.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faTv } from '@fortawesome/free-solid-svg-icons';

const IMAGE_API = "https://image.tmdb.org/t/p/w500";

const Movie = (props) => {
  const {id, title, poster_path, isFav, toWatch, toggleFavorite, toggleWatchlist} = props;
  const favClass = isFav ? 'favorite-icon fav' : 'favorite-icon';
  const watchlistClass = toWatch ? 'watchlist-icon added' : 'watchlist-icon'
  return <div className="Movie" data-testid="Movie">
    <div onClick={() => toggleFavorite(id, !isFav)} className={favClass}><FontAwesomeIcon icon={faStar}/></div>
    <div onClick={() => toggleWatchlist(id, !toWatch)} className={watchlistClass}><FontAwesomeIcon icon={faTv}/></div>
    <div className="image">
      <img src={IMAGE_API + poster_path}></img>
    </div>
    <div className="title">{title}</div>
  </div>
};

export default Movie;
