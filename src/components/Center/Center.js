import React, { useState, useEffect } from 'react';
import './Center.css';
import Movie from '../Movie/Movie.js';
import {markAsFavorite, getFavMovies, getWatchList, addToWatchList} from '../../api/API_Methods.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faTv } from '@fortawesome/free-solid-svg-icons';

const Center = (props) => {
  const {movies, getNextPage} = props;
  const [favMovies, setFavMovies] = useState([]);
  const [watchlist, setWatchlist] = useState([]);
  const [showMovies, setShowMovies] = useState(movies);
  const [view, setView] = useState('all');

  useEffect(() => {
    Promise.all([getFavMovies(), getWatchList()])
           .then(response => {
              const [favRes, watchlistRes] = response;
              if(favRes.status_code == 3 || watchlistRes.status_code == 3) {
                // if not authenticated
                delete sessionStorage.session_id;
                delete localStorage.request_token;
                window.location.reload();
              }
              let tempFavIds = [];
              let tempWatchlist = [];
              favRes.results.forEach((movie) => {
                tempFavIds.push(movie.id);
              });
              watchlistRes.results.forEach(movie => {
                tempWatchlist.push(movie.id);
              });
              setFavMovies(tempFavIds);
              setWatchlist(tempWatchlist);
           }).catch(() => {
             console.log('Not authenticated')
           })
  }, [])

  useEffect(async () => {
    if (view == 'favorites') {
      const favorites = await getFavMovies();
      setShowMovies(favorites.results);
    } else if (view == 'watchlist') {
      const listedToWatch = await getWatchList();
      setShowMovies(listedToWatch.results);
    } else {
      setShowMovies(movies);
    }
  }, [view, movies]);

  const setAsFavorite = async (movieId, isFav) => {
    // const isFav = favMovies.indexOf(movieId) == -1 ? false : true;
    const setRes = await markAsFavorite(movieId, isFav);
    if (setRes.success) {
      if (isFav) {
        setFavMovies([...favMovies, movieId]);
        return;
      }
      const favList = favMovies.filter(id => {
        return movieId !== id;
      });
      setFavMovies(favList);
    }
  }

  const toggleWatchlist = async (movieId, toWatch) => {
    // const toWatch = watchlist.indexOf(movieId) == -1 ? false : true;
    const setRes = await addToWatchList(movieId, toWatch);
    if (setRes.success) {
      if (toWatch) {
        setWatchlist([...watchlist, movieId]);
        return;
      }
      const toWatchList = watchlist.filter(id => {
        return movieId !== id;
      });
      setWatchlist(toWatchList);
    }
  }

  const allClass = view === 'all' ? "filter-tab selected" : 'filter-tab';
  const favClass = view === 'favorites' ? "filter-tab selected" : 'filter-tab';
  const watchClass = view === 'watchlist' ? "filter-tab selected" : 'filter-tab';
  return <>
    <div className="tab-container">
      <div className={allClass} onClick={() => setView('all')}>
        All
      </div>
      <div className={favClass} onClick={() => setView('favorites')}>
        <FontAwesomeIcon icon={faStar}/>
      </div>
      <div className={watchClass} onClick={() => setView('watchlist')}>
        <FontAwesomeIcon icon={faTv}/>
      </div>
    </div>
    <div className="center">
      <div className="movies-container">
        {showMovies.map((movie) => {
          const isFav = favMovies.indexOf(movie.id) == -1 ? false : true;
          const toWatch = watchlist.indexOf(movie.id) == -1 ? false : true;
          return <Movie key={movie.id} {...movie} isFav={isFav} toWatch={toWatch} toggleFavorite={setAsFavorite} toggleWatchlist={toggleWatchlist}/>
        })}
      </div>
      <div className="more-btn"><button onClick={() => getNextPage()}>More</button></div>
    </div>
  </>
};

export default Center;
