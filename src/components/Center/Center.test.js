import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Center from './Center';

describe('<Center />', () => {
  test('it should mount', () => {
    render(<Center />);
    
    const center = screen.getByTestId('Center');

    expect(center).toBeInTheDocument();
  });
});