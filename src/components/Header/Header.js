import React from 'react';
import './Header.css';
import Search from '../Search/Search.js';

const Header = (props) => {
  const {search} = props;
  return <div className="Header" data-testid="Header">
    <Search search={search}/>
  </div>
};

export default Header;
